///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file refTime.c
//
// Print reference time
//
//
// @author Colby Kagamida <colbykag@hawaii.edu>
// @date   04 Feb 2021
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "refTime.h"

//print difference between reference time and current time
void printDiff(int rYear, int cYear, int rDay, int cDay, int rHour, 
      int cHour, int rMin, int cMin, int rSec, int cSec){
   int oSec = cSec - rSec;
   if(oSec < 0){
      cMin--;
      oSec += 60;
   }
   int oMin = cMin - rMin;
   if(oMin < 0){
      cHour--;
      oMin += 60;
   }
   int oHour = cHour - rHour;
   if(oHour < 0){
      cDay--;
      oHour += 24;
   }
   int oDay = cDay - rDay;
   if(oDay < 0) {
      cYear--;
      oDay += 365;
   }
   int oYear = cYear - rYear;
   if(oYear < 0){
      printf("invalid reference date\n");
      return;
   }
   printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n", oYear, 
         oDay, oHour, oMin, oSec);
}
