///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Colby Kagamida <colbykag@hawaii.edu>
// @date   04 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "refTime.h"
#define DEBUG 1

int main(int argc, char* argv[]) {
   time_t timeNow;
   int sec = refSec;
   int min = refMin;
   int hour = refHour;
   int day = refyDay;
   int year = refYear;
   int mDay = refmDay;
   int pmTime = refHour;
   struct tm * timeInfo;

   if(pmTime > 12)
      pmTime -= 12;

   printf("Reference time: Mon Jun %d %d:%d:%d PM HST %d\n\n",mDay, pmTime, 
         min, sec, year);

   while(DEBUG)
   {
      time (&timeNow);
      timeInfo = localtime (&timeNow);
      //printf("Current local time and date: %s\n", asctime(timeInfo));
      printDiff(year, (int)(timeInfo->tm_year+1900), day, 
            (int)(timeInfo->tm_yday+1), hour, (int)(timeInfo->tm_hour), 
            min, (int)(timeInfo->tm_min), sec, (int)(timeInfo->tm_sec));
      sleep(1);
   }
   return 0;
}
