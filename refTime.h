///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file refTime.h
//
// Defines reference time
//
//
// @author Colby Kagamida <colbykag@hawaii.edu>
// @date   04 Feb 2021
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define refSec 11
#define refMin 20
#define refHour 23
#define refmDay 17
#define refMon 6
#define refYear 2019
#define refyDay 168

//print difference between reference time and current time
void printDiff(int rYear, int cYear, int rDay, int cDay, int rHour, 
      int cHour, int rMin, int cMin, int rSec, int cSec);
