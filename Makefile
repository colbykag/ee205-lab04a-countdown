###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04a - Countdown
#
# @file    Makefile
# @version 1.0
#
# @author @Colby Kagamida <colbykag@hawaii.edu>
# @brief  Lab 04a - Countdown - EE 205 - Spr 2021
# @date   @ 04_Feb_2021
###############################################################################

all: countdown

countdown.o: countdown.c refTime.c refTime.h
	gcc -c countdown.c refTime.c

countdown: countdown.o refTime.o
	gcc -o countdown countdown.o refTime.o

clean:
	rm -f *.o countdown
